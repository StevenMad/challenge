package level2;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.gherkin.internal.com.eclipsesource.json.JsonObject;
import level2.entities.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import level2.Service.CompanyService;
import level2.Service.JsonService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestLevel2 {

    String json;
    CompanyService companyService = new CompanyService();
    JsonService jsonService = new JsonService();
    String expectedOutput;

    @Before
    public void getJsonInput() throws FileNotFoundException, IOException {
        String file = "backend/Level2/src/test/java/level2/input.json";
        List<String> jsonFile = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        reader.lines().forEach(jsonFile::add);
        json = "";
        for (String s : jsonFile) {
            json += s;
        }
        file = "backend/Level2/src/test/java/level2/output-expected.json";
        jsonFile = new ArrayList<String>();
        reader = new BufferedReader(new FileReader(file));
        reader.lines().forEach(jsonFile::add);
        expectedOutput = "";
        for (String s : jsonFile) {
            expectedOutput += s;
        }
    }

    @Test
    public void testFromInputPerformBalance() throws Exception {
        JSONObject jsonObject = new JSONObject(json);
        ArrayList<Company> companies = new ArrayList<>();
        ArrayList<User> users = new ArrayList<>();
        ArrayList<Wallet> wallets = new ArrayList<>();
        ArrayList<Distribution> distributions = new ArrayList<>();
        companies = initCompanies(jsonObject.getJSONArray("companies"));
        users = initUsers(jsonObject.getJSONArray("users"));
        wallets = initWallet(jsonObject.getJSONArray("wallets"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        companyService.giveMoney(1, companies.get(0), users.get(0), new UserWallet(1, 50), simpleDateFormat.parse("2020-09-16"), distributions);
        companyService.giveMoney(2, companies.get(0), users.get(1), new UserWallet(1, 100), simpleDateFormat.parse("2020-08-01"), distributions);
        companyService.giveMoney(3, companies.get(1), users.get(2), new UserWallet(1, 1000), simpleDateFormat.parse("2020-05-01"), distributions);
        companyService.giveMoney(3, companies.get(0), users.get(0), new UserWallet(2, 250), simpleDateFormat.parse("2020-05-01"), distributions);
        JSONObject actualOutpt = jsonService.buildJson(companies, users, distributions);
        JSONObject expectedJSON = new JSONObject(expectedOutput);
        ObjectMapper comparator = new ObjectMapper();
        assertThat(comparator.readTree(actualOutpt.toString()), is(comparator.readTree(expectedOutput)));

    }

    private ArrayList<Company> initCompanies(JSONArray companiesJSON) throws Exception {
        ArrayList<Company> companies = new ArrayList<>();
        for (int i = 0; i < companiesJSON.length(); i++) {
            JSONObject company = companiesJSON.getJSONObject(i);
            companies.add(new Company(company.getInt("id"), company.getString("name"), company.getInt("balance")));
        }
        return companies;
    }

    private ArrayList<User> initUsers(JSONArray usersJSON) throws Exception {
        ArrayList<User> users = new ArrayList<>();
        for (int i = 0; i < usersJSON.length(); i++) {
            JSONObject user = usersJSON.getJSONObject(i);
            JSONArray wallet = user.getJSONArray("balance");
            User userObject = new User(user.getInt("id"));
            for (int j = 0; j < wallet.length(); j++) {
                JSONObject walletJSON = wallet.getJSONObject(i);
                UserWallet userWallet = new UserWallet(user.getInt("id"));
                userWallet.setWallet_id(walletJSON.getInt("wallet_id"));
                userWallet.setAmount(walletJSON.getInt("amount"));
                userObject.addBalance(userWallet);
            }
            users.add(userObject);
        }
        return users;
    }

    private ArrayList<Wallet> initWallet(JSONArray walletJSON) throws Exception {
        ArrayList<Wallet> wallets = new ArrayList<>();
        for (int i = 0; i < walletJSON.length(); i++) {
            JSONObject wallet = walletJSON.getJSONObject(i);
            wallets.add(new Wallet(wallet.getInt("id"), wallet.getString("name"), wallet.getString("type")));
        }
        return wallets;
    }


}
