package level2.entities;

import javax.persistence.Entity;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
public class User {

    Integer id;
    Map<Integer, UserWallet> balance;

    public User(Integer userId, UserWallet balance) {
        this.id = userId;
        this.balance = new HashMap<Integer, UserWallet>();
        this.balance.put(balance.wallet_id, balance);
    }

    public User(Integer userId){
        this.id = userId;
        balance = new HashMap<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setBalance(UserWallet balance) {
        UserWallet wallet = this.balance.get(balance.wallet_id);
        if(wallet==null){
            addBalance(balance);
        }else{
            wallet.setAmount(wallet.getAmount() + balance.getAmount());
        }
    }

    public void addBalance(UserWallet balance){
        this.balance.put(balance.getWallet_id(),balance);
    }


    public String toJsonString() {
        StringBuilder sb = new StringBuilder();
        String userJson = String.format("{\"id\":%d,\"balance\":[",id);
        sb.append(userJson);
        balance.forEach((key,balance)-> {
            String balanceString = balance.toJsonString()+",";
            sb.append(balanceString);
        });
        sb.deleteCharAt(sb.length()-1);
        sb.append("]}");
        return sb.toString();
    }
}
