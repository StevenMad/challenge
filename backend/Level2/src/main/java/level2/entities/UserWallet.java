package level2.entities;

public class UserWallet {
    Integer user_id;
    Integer wallet_id;
    Integer amount;

    public UserWallet(Integer user_id) {
        this.user_id = user_id;
    }

    public UserWallet(Integer wallet_id,Integer amount) {
        this.wallet_id = wallet_id;
        this.amount = amount;
    }

    public Integer getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(Integer wallet_id) {
        this.wallet_id = wallet_id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String toJsonString(){
        return String.format("{\"wallet_id\":%d,\"amount\":%d}",wallet_id,amount);
    }
}
