package level2.entities;

import java.util.Date;

public class Distribution {

    Integer id;
    Integer amount;
    Date start_date;
    Date end_date;
    Integer wallet_id;
    Integer company_id;
    Integer user_id;

    public Distribution(Integer id, UserWallet balance, Date startDate, Date endDate, Integer company_id, Integer user_id) {
        this.id= id;
        this.amount = balance.getAmount();
        this.start_date = startDate;
        this.end_date = endDate;
        this.company_id = company_id;
        this.user_id = user_id;
        this.wallet_id = balance.getWallet_id();
    }

    public String toJsonString() {
        return String.format("{\"id\":%d,\"amount\":%d,\"start_date\":\"%s\",\"end_date\":\"%s\",\"company_id\":%d,\"user_id\":%d,\"wallet_id\":%d}", id, amount, start_date, end_date, company_id, user_id,wallet_id);
    }
}
