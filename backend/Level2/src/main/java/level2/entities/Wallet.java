package level2.entities;

import javax.persistence.Entity;

@Entity
public class Wallet{

    Integer id;
    String name;
    String type;

    public Wallet(Integer id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }
}