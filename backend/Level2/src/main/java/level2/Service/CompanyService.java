package level2.Service;

import level2.entities.Company;
import level2.entities.Distribution;
import level2.entities.User;
import level2.entities.UserWallet;
import level2.repository.CompanyRepository;
import level2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserRepository userRepository;

    public void giveMoney(Integer distributionId, Company company, User user, UserWallet balance, Date startDate, List<Distribution> distributions) throws ParseException {
        company.setBalance(company.getBalance() - balance.getAmount());
        user.setBalance(balance);
        updateDistribution(distributionId, distributions, company.getId(), user.getId(), startDate, balance);
    }

    private void updateDistribution(Integer distributionId, List<Distribution> distributions, Integer companyId, Integer userId, Date startDate, UserWallet balance) throws ParseException {
        Date endDate = getEndDate(startDate, balance);
        distributions.add(new Distribution(
                distributionId,
                balance, startDate, endDate, companyId, userId
        ));
    }

    private Date getEndDate(Date startDate, UserWallet wallet) throws ParseException {
        //should call a repository to get wallets type instead let's just say 1=GIFT and 2=FOOD
        if (wallet.getWallet_id() == 1) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.add(Calendar.DATE, 364);
            return cal.getTime();
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            Integer year = cal.get(Calendar.YEAR) + 1;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            cal.setTime(simpleDateFormat.parse(year + "-02-01"));
            int lastDayOfFeb = cal.getActualMaximum(Calendar.DATE);
            return simpleDateFormat.parse(year + "-02-" + lastDayOfFeb);
        }

    }
}
