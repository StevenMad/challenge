package level2.Service;

import level2.entities.Company;
import level2.entities.Distribution;
import level2.entities.User;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonService {

    public JSONObject buildJson(ArrayList<Company> companies, ArrayList<User> users, List<Distribution> distributionList) {
        JSONObject jsonResult = new JSONObject();
        JSONArray companiesArray = new JSONArray();
        companies.forEach((company) -> {
            companiesArray.put(new JSONObject(company.toJsonString()));
        });
        JSONArray usersArray = new JSONArray();
        users.forEach((user) -> {
            usersArray.put(new JSONObject(user.toJsonString()));
        });
        JSONArray distributionArray = new JSONArray();
        distributionList.forEach((distribution) -> {
            distributionArray.put(new JSONObject(distribution.toJsonString()));
        });
        jsonResult.put("companies", companiesArray);
        jsonResult.put("users", usersArray);
        jsonResult.put("distributions",distributionArray);
        return jsonResult;
    }
}
