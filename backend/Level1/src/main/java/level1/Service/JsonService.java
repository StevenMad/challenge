package level1.Service;

import level1.entities.Company;
import level1.entities.Distribution;
import level1.entities.User;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonService {

    public JSONObject buildJson(ArrayList<Company> companies, ArrayList<User> users, List<Distribution> distributionList) {
        JSONObject jsonResult = new JSONObject();
        JSONArray companiesArray = new JSONArray();
        companies.forEach((company) -> {
            companiesArray.put(new JSONObject(company.toJsonString()));
        });
        JSONArray usersArray = new JSONArray();
        users.forEach((user) -> {
            usersArray.put(user.toJsonString());
        });
        JSONArray distributionArray = new JSONArray();
        distributionList.forEach((distribution) -> {
            distributionArray.put(distribution.toJsonString());
        });
        jsonResult.put("companies", companiesArray);
        jsonResult.put("users", usersArray);
        jsonResult.put("distributions",distributionArray);
        return jsonResult;
    }
}
