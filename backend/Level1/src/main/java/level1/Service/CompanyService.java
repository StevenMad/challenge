package level1.Service;

import level1.entities.Company;
import level1.entities.Distribution;
import level1.entities.User;
import level1.repository.CompanyRepository;
import level1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserRepository userRepository;

    public void giveMoney(Integer distributionId, Company company, User user, Integer balance, Date startDate, List<Distribution> distributions) {
        company.setBalance(company.getBalance() - balance);
        user.setBalance(user.getBalance() + balance);
        updateDistribution(distributionId, distributions, company.getId(), user.getId(), startDate, balance);
    }

    private void updateDistribution(Integer distributionId, List<Distribution> distributions, Integer companyId, Integer userId, Date startDate, Integer balance) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.add(Calendar.DATE, 364);
        Date endDate = cal.getTime();
        distributions.add(new Distribution(
                distributionId,
                balance, startDate, endDate, companyId, userId
        ));
    }
}
