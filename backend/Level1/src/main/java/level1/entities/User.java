package level1.entities;

import javax.persistence.Entity;

@Entity
public class User {

    Integer id;
    Integer balance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public User(Integer userId, Integer balance) {
        this.id = userId;
        this.balance = balance;
    }

    public String toJsonString() {
        return String.format("{\"id\":%d,\"balance\":%d}", id, balance);
    }
}
