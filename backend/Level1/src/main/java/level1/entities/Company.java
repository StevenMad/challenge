package level1.entities;

import javax.persistence.Entity;

@Entity
public class Company {


    Integer id;
    String companyName;
    Integer balance;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Company(Integer id, String name, Integer balance) {
        this.id = id;
        this.companyName = name;
        this.balance = balance;
    }

    public String toJsonString() {
        return String.format("{\"id\":%d,\"name\":\"%s\",\"balance\":%d}", id, companyName, balance);
    }

}
