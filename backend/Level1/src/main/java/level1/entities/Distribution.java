package level1.entities;

import java.util.Date;

public class Distribution {

    Integer id;
    Integer amount;
    Date start_date;
    Date end_date;
    Integer company_id;
    Integer user_id;

    public Distribution(Integer id, Integer amount, Date startDate, Date endDate, Integer company_id, Integer user_id) {
        this.id= id;
        this.amount = amount;
        this.start_date = startDate;
        this.end_date = endDate;
        this.company_id = company_id;
        this.user_id = user_id;
    }

    public String toJsonString() {
        return String.format("{\"id\":%d,\"amount\":%d,\"start_date\":%s,\"end_date\":%s,\"company_id\":%d,\"user_id\":%d}", id, amount, start_date, end_date, company_id, user_id);
    }
}
