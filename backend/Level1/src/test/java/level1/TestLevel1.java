package level1;

import com.fasterxml.jackson.databind.ObjectMapper;
import level1.Service.CompanyService;
import level1.Service.JsonService;
import level1.entities.Company;
import level1.entities.Distribution;
import level1.entities.User;
import level1.repository.CompanyRepository;
import level1.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.comparator.JSONComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

public class TestLevel1 {

    String json;
    CompanyService companyService = new CompanyService();
    JsonService jsonService = new JsonService();
    String expectedOutput;

    @Before
    public void getJsonInput() throws FileNotFoundException, IOException {
        String file = "src/test/java/level1/input.json";
        List<String> jsonFile = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        reader.lines().forEach(jsonFile::add);
        json = "";
        for (String s : jsonFile) {
            json += s;
        }
        file = "src/test/java/level1/output-expected.json";
        jsonFile = new ArrayList<String>();
        reader = new BufferedReader(new FileReader(file));
        reader.lines().forEach(jsonFile::add);
        expectedOutput = "";
        for (String s : jsonFile) {
            expectedOutput += s;
        }
    }

    @Test
    public void testFromInputPerformBalance() throws Exception {
        JSONObject jsonObject = new JSONObject(json);
        ArrayList<Company> companies = new ArrayList<>();
        ArrayList<User> users = new ArrayList<>();
        ArrayList<Distribution> distributions = new ArrayList<>();
        companies = initCompanies(jsonObject.getJSONArray("companies"));
        users = initUsers(jsonObject.getJSONArray("users"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        companyService.giveMoney(1, companies.get(0), users.get(0), 50, simpleDateFormat.parse("2020-09-16"), distributions);
        companyService.giveMoney(2, companies.get(0), users.get(1), 100, simpleDateFormat.parse("2020-08-01"), distributions);
        companyService.giveMoney(3, companies.get(1), users.get(2), 1000, simpleDateFormat.parse("2020-05-01"), distributions);
        JSONObject actualOutpt = jsonService.buildJson(companies, users, distributions);
        JSONObject expectedJSON = new JSONObject(expectedOutput);
        ObjectMapper comparator = new ObjectMapper();
        assertThat(comparator.readTree(actualOutpt.toString()), is(comparator.readTree(expectedOutput)));

    }

    private ArrayList<Company> initCompanies(JSONArray companiesJSON) throws Exception {
        ArrayList<Company> companies = new ArrayList<>();
        for (int i = 0; i < companiesJSON.length(); i++) {
            JSONObject company = companiesJSON.getJSONObject(i);
            companies.add(new Company(company.getInt("id"), company.getString("name"), company.getInt("balance")));
        }
        return companies;
    }

    private ArrayList<User> initUsers(JSONArray usersJSON) throws Exception {
        ArrayList<User> users = new ArrayList<>();
        for (int i = 0; i < usersJSON.length(); i++) {
            JSONObject user = usersJSON.getJSONObject(i);
            users.add(new User(user.getInt("id"), user.getInt("balance")));
        }
        return users;
    }


}
